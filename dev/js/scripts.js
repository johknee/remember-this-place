/*

*/

var app = {};


// MODELS ========================================

	app.Place = Backbone.Model.extend({
		defaults: {
			title: 'A Place',
			latitude: null,
			longitude: null,
			distance: 0,
			weather: ""
		}
	});


// COLLECTIONS ===================================

	app.LocationsList = Backbone.Collection.extend({
		model: app.Place,
		localStorage: new Store("rememberthisplace")
    });

    app.locationsList = new app.LocationsList();


// VIEWS =========================================

	app.PlaceView = Backbone.View.extend({
		tagName: 'li',
		events: {
			'click .content'		: 'openLocation',
			'click .edit_activate' 	: 'editLocation',
			'click .save-new' 		: 'saveTitle',
			'keyup input' 			: 'saveTitleKey',
			'click .delete_activate': 'deleteLocation',
			'click .delete_sure' 	: 'destroyLocation',
			'click .zoom-in'		: 'zoomIn',
			'click .zoom-out'		: 'zoomOut'
		},
		openLocation: function() {
			if(!this.$el.hasClass("editing"))
			{
				if(this.$el.hasClass("active"))
				{
					this.$el.removeClass("active");

					app.appView.trigger("mapClose");
				}
				else
				{
					$(".locations > li.active").removeClass("active editing");
					this.$el.addClass("active");
					app.appView.trigger("mapOpen");

					app.appView.openLocation(this.model.get("latitude"),this.model.get("longitude"));

					var ind = app.locationsList.length - app.locationsList.indexOf(this.model);

					var hei = ((ind-1) * this.$el.height()) + $("#remember-btn").outerHeight();

					$("html,body").animate({scrollTop : hei},400);
				}
			}
		},
		editLocation: function() {
			if(this.$el.hasClass("editing"))
			{
				this.$el.removeClass("editing");
				this.input.blur();
			}
			else
			{
				this.$el.addClass("editing");
				this.input.focus();
			}
		},
		saveTitleKey: function(e) {
			if(e.keyCode==13)
			{
				this.saveTitle();
			}
		},
		saveTitle: function() {
			var value = this.input.val().trim();
			this.$el.removeClass("editing");
			this.input.blur();
			this.weatherGet = false;
	        if(value) {
	          this.model.save({title: value});
	        }
		},
		deleteLocation: function() {
			if(this.$(".delete_sure").hasClass("show"))
			{
				this.$el.removeClass("deleter");
				this.$(".delete_sure").removeClass("show");
			}
			else
			{
				this.$el.addClass("deleter");
				this.$(".delete_sure").addClass("show");
			}
		},
		destroyLocation: function() {
			this.preRemove();
		},
		initialize: function() {
			this.template = _.template($('#place-template').html());
			this.model.on('change', this.render, this);
			this.model.on('update', this.variables, this);
			this.model.on('destroy', this.remove, this);
		},
		calcDistance : function() {
			var modelCo = L.latLng(this.model.get("latitude"), this.model.get("longitude")),
				actualCo = L.latLng(app.appView.geolocation.coords.latitude, app.appView.geolocation.coords.longitude);

			var dist = ((modelCo.distanceTo(actualCo)) / 1000) * 0.621371192;
				dist = dist.toFixed(2),
				dist = dist+"mi";

			this.$(".distance_set").html('<span class="icon distance"></span> '+dist);
		},
		getWeather : function() {
			if(!this.weatherGet)
			{
				var self = this;
				$.getJSON("http://api.openweathermap.org/data/2.5/weather",{lat:this.model.get("latitude"),lon:this.model.get("longitude"),units:"metric"},function(data)
				{
					self.weatherGet = true;

					self.$(".weather_set").html('<span class="icon weather"></span> '+data.weather[0].main+' '+data.main.temp.toFixed(0)+'\xB0C');
				});
			}
		},
		variables: function() {
			this.calcDistance();
			this.getWeather();
		},
		preRemove: function() {
			var self = this;

			$("html,body").animate({scrollTop : 0},400);

			app.appView.trigger("mapClose");

			this.$el.slideUp(500,function() {
				self.model.destroy();
			});
		},
		zoomIn: function() {
			app.appView.trigger("zoomIn");
		},
		zoomOut: function() {
			app.appView.trigger("zoomOut");
		},
		render: function(){
			this.variables();

			this.$el.html(this.template(this.model.toJSON()));
			this.input = this.$el.find("input");
			return this; // enable chained calls
		}
	});


	app.AppView = Backbone.View.extend({
		el: '#app',
		initialize: function () {
			this.getLocation();
			this.list = this.$(".locations");
			this.open = false;
			this.zoom = 13;
			this.initial = true;

			app.locationsList.on('add', this.addOne, this);
        	app.locationsList.on('reset', this.addAll, this);
        	this.on('mapOpen', this.mapOpener, this);
        	this.on('mapClose', this.mapCloser, this);
        	this.on('zoomIn', this.zoomIn, this);
        	this.on('zoomOut', this.zoomOut, this);
		},
		events: {
			'click #remember-btn' 	: 'promptLocation',
			'click .overlay'		: 'overlay',
			'click #save-btn' 		: 'addLocation',
			'keyup #save_box input' : 'addLocationKey'
		},
		getLocation: function() {
			var self = this;

			this.$("#loading .message").text("This app requires the use of your location, and cannot run without it");
			if (Modernizr.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position){ self.showMap(position) },function(err){ self.handleLocationError(err) });
			} else {
				this.$("#loading .message").text("It doesn't look like location is supported on your device/browser, which means we can't do our job, sorry!");
			}
		},
		showMap: function(position) {
			this.$("#loading .message").text("Loading...");

			this.updateLocation(position);
			this.initiateMap();
		},
		updateLocation: function(position) {
			this.geolocation = position;
			this.$(".coord-show").text(this.formatCoord());

			if(typeof this.map !== "undefined")
			{
				if(!this.open)
				{
					this.map.setView([this.geoCater(),this.geolocation.coords.longitude],13);
				}
				else
				{
					this.updateLine();
				}
				this.marker.setLatLng([this.geolocation.coords.latitude,this.geolocation.coords.longitude]);
				
				for(var i = 0; i<app.locationsList.length; i++)
				{
					var curr = app.locationsList.at(i);

					curr.trigger("update");
				}
			}

			if($("#error_box").length==1)
			{
				$("#error_box").removeClass("show");

				app.appView.overlay(false);
				setTimeout(function()
				{
					$("#error_box").remove();
				},500);
			}
		},
		formatCoord: function() {
			var lat = String(this.geolocation.coords.latitude),
				lng = String(this.geolocation.coords.longitude);

			lat = lat.substr(0,5);
			lng = lng.substr(0,5);

			return lat+","+lng;
		},
		handleLocationError: function(err) {
			if(err.code==1)
			{
				this.$("#loading .message").html("We are sorry to see you didn't want to give your location. Thats fine though, its not like we <u>cared or anything</u>");
			}
			else
			{
				this.$("#loading .message").html("We are having trouble connecting, whats your signal like?")
				var self = this;
				setTimeout(function()
				{
					navigator.geolocation.getCurrentPosition(function(position){ self.showMap(position) },function(err){ self.handleLocationError(err) });
				},1000);
			}
		},
		handleOpenError: function(err) {
			if($("#error_box").length==0)
			{
				var html = '<div id="error_box" class="modal">
						        <p><span class="icon connection"></span><br>We seem to have lost where you are! Please reconnect to carry on using this app</p>
						    </div>';

				$("body").prepend(html);
				app.appView.overlay(true);

				setTimeout(function()
				{
					$("#error_box").addClass("show");
				},50);
			}
			var self = this;
			setTimeout(function()
			{
				navigator.geolocation.getCurrentPosition(function(position){ self.updateLocation(position) },function(err){ self.handleOpenError(err) });
			},1000);
		},
		initiateMap: function() {
			var self = this;

			L.mapbox.accessToken = 'pk.eyJ1IjoianRoYXdtZSIsImEiOiJlM2RiMTU5YTIyOGVmZmFmNjcyMzk4ZTU1YzY5ZTA0OCJ9.t0-fjjgqQGlqUzPTrZ4qGw';
		    // Create a map in the div #map
		    this.map = L.mapbox.map('map', 'jthawme.245f64f8', {
		        zoomControl: false
		    }).setView([this.geolocation.coords.latitude,this.geolocation.coords.longitude],this.zoom);

		    this.youIcon = L.icon({
			    iconUrl: 'assets/images/marker_alt.png',
			    iconRetinaUrl: 'assets/images/marker_alt@2x.png',
			    iconSize: [36,48],
			    iconAnchor: [18, 48]
			});
		    this.destIcon = L.icon({
			    iconUrl: 'assets/images/marker.png',
			    iconRetinaUrl: 'assets/images/marker@2x.png',
			    iconSize: [36,48],
			    iconAnchor: [18, 48]
			});

		    this.marker = L.marker([this.geolocation.coords.latitude,this.geolocation.coords.longitude], {icon: this.youIcon}).addTo(this.map);

		    this.map.dragging.disable();
			this.map.touchZoom.disable();
			this.map.doubleClickZoom.disable();
			this.map.scrollWheelZoom.disable();

			if (this.map.tap) this.map.tap.disable();


			this.launchAfter();
		},
		launchAfter: function() {
			var self = this;

			this.timer = setInterval(function() {
			      navigator.geolocation.getCurrentPosition(function(position){ self.updateLocation(position) },function(err){ self.handleOpenError(err) });
			 }, 1000);

			$("#top").show();

			$("#loading").fadeOut(500);

			setTimeout(function() {
				$("#map").addClass("show");
			},500);

			setTimeout(function() {
				$("#top,header").addClass("show");
				self.map.setView([self.geoCater(),self.geolocation.coords.longitude],13);
				app.locationsList.fetch();
			},1500);
		},
		geoCater: function() {
			if(this.open)
			{
				return this.geolocation.coords.latitude;
			}
			else
			{
				return this.geolocation.coords.latitude;
			}
		},
		overlay: function(bool) {
			if(typeof bool.which !== "undefined"||!bool)
			{
				this.$(".overlay").fadeOut(500);
				this.$(".modal").removeClass("show");
			}
			else
			{
				this.$(".overlay").fadeIn(500);
			}
		},
		promptLocation: function() {
			this.overlay(true);
			this.$("#save_box").addClass("show");
			//$("html,body").animate({scrollTop:$(window).height()*0.3},500);
			this.$("#save_box input").focus();
		},
		addLocationKey: function(e) {
			if(e.keyCode==13)
			{
				this.addLocation();
			}
		},
		addLocation: function() {
			if(this.$("#save_box input").val().trim()=="")
			{
				var title = "A cool place";
			}
			else
			{
				var title = $("#save_box input").val().trim();
			}

			$("#save_box input").val("").blur();

			var place = {
				title: title,
				latitude: this.geolocation.coords.latitude,
				longitude: this.geolocation.coords.longitude
			};

			app.locationsList.create(place);
			this.overlay(false);
		},
		openLocation: function(lat,lon) {

			if(this.dest)
			{
				this.map.removeLayer(this.line);
				this.map.removeLayer(this.targetMarker);
			}
			this.zoom = 13;
			this.map.setView([lat,lon],this.zoom);
		    this.targetMarker = L.marker([lat,lon], {icon: this.destIcon}).addTo(this.map);

		    var curr = L.latLng(this.geolocation.coords.latitude,this.geolocation.coords.longitude);
		    this.dest = L.latLng(lat,lon);

		    this.line = L.polyline([curr,this.dest]).addTo(this.map);
		},
		updateLine: function() {
			this.map.removeLayer(this.line);
			var curr = L.latLng(this.geolocation.coords.latitude,this.geolocation.coords.longitude);
			this.line = L.polyline([curr,this.dest]).addTo(this.map);
		},
		addOne: function(place){
			var view = new app.PlaceView({model: place});

			var el = view.render().el
			this.list.prepend(el);

			if(!this.initial)
			{
				view.openLocation();
			}

			$(el).addClass("show");
		},
		addAll: function(){
			this.list.html(''); // clean the todo list
			app.locationsList.each(this.addOne, this);
			this.initial = false;
		},
		mapCloser: function() {
			$("html").removeClass("map-open");
			$("html,body").animate({scrollTop : 0},400);
			this.open = false;

			this.map.removeLayer(this.line);
			this.map.removeLayer(this.targetMarker);

		},
		mapOpener: function() {
			$("html").addClass("map-open");
			this.open = true;
		},
		zoomIn: function() {
			this.zoom += 1;

			if(this.zoom>=18)
			{
				this.zoom = 18;
			}
			this.map.setZoom(this.zoom);
		},
		zoomOut: function() {
			this.zoom -= 1;

			if(this.zoom<2)
			{
				this.zoom = 2;
			}
			this.map.setZoom(this.zoom);
		}
	});

$(document).ready(function()
{
	app.appView = new app.AppView(); 
});