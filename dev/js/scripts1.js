$(document).ready(function() {
    L.mapbox.accessToken = 'pk.eyJ1IjoianRoYXdtZSIsImEiOiJlM2RiMTU5YTIyOGVmZmFmNjcyMzk4ZTU1YzY5ZTA0OCJ9.t0-fjjgqQGlqUzPTrZ4qGw';
    // Create a map in the div #map
    var map = L.mapbox.map('map', 'jthawme.245f64f8', {
        zoomControl: false
    }).setView([51.4648139,-0.1530361],13);

    var youIcon = L.icon({
	    iconUrl: 'assets/images/marker_alt.png',
	    iconRetinaUrl: 'assets/images/marker_alt@2x.png',
	    iconSize: [36,48]
	});
    var destIcon = L.icon({
	    iconUrl: 'assets/images/marker.png',
	    iconRetinaUrl: 'assets/images/marker@2x.png',
	    iconSize: [36,48]
	});

    L.marker([51.4718139,-0.1530361], {icon: youIcon}).addTo(map);

	map.dragging.disable();
	map.touchZoom.disable();
	map.doubleClickZoom.disable();
	map.scrollWheelZoom.disable();

	if (map.tap) map.tap.disable();

	requestAnimFrame(animloop);

	setTimeout(function() {
		$("#map").addClass("show");
	},500);

	setTimeout(function() {
		$("#top,header").addClass("show");
	},1500);

	setTimeout(function() {
		$(".locations > li").addClass("show");
	},2500);

	$(".locations > li .content").click(function(e) {
		e.preventDefault();

		var parent = $(this).parent();

		if(parent.hasClass("active"))
		{
			parent.removeClass("active");
			$("html").removeClass("map-open");
		}
		else
		{
			$(".locations > li.active").removeClass("active");
			parent.addClass("active");
			$("html").addClass("map-open");

			var ind = $(".locations > li .content").index(this);

			$("html,body").animate({scrollTop : ind*$(".locations > li").height()},400);
		}
		return false;
	});

	$(".locations > li .edit").click(function(e) {
		e.preventDefault();
		$(this).parents(".individual").toggleClass("editing");
		return false;
	});
});

function animloop(){
	
	var scrollT = $(window).scrollTop(),
		windH = $(window).height(),
		windW = $(window).width(),
		scrollB = scrollT+windH;

	requestAnimFrame(animloop);
}