var gulp = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    plugins = gulpLoadPlugins(),
	neat = require('node-neat').includePaths,
	mainBowerFiles = require('main-bower-files'),
	del = require('del');
	
var paths = {
		js : {
			origin 	: "dev/js/*.js",
			dest	: "assets",
			path	: "dev/js"
		},
		css : {
			origin 	: "dev/**/*.scss",
			dest	: "assets",
			path	: "dev/css"
		},
		images : {
			origin 	: "dev/images/**",
			dest	: "assets/images",
		},
		svg : {
			origin 	: "dev/images/**/*.svg",
			dest	: "assets/images",
		},
		bower : {
			origin 	: "dev/js/bower",
			dest 	: "assets/js/bower"
		}
	}

// GULP TASKS	
// ==========================================================
	
	gulp.task('clean-images', function () {
		del([
			paths.images.dest+'/**/*',
			'!'+paths.images.dest+'/.gitkeep'
		], function(){});
	});

	gulp.task('js', function () {
		
		gulp.src([paths.js.origin, '!'+paths.js.path+'/bower'])
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.plumber())
			.pipe(plugins.uglify())
			.pipe(plugins.sourcemaps.write())
			.pipe(gulp.dest(paths.js.dest+'/js'))
			.pipe(plugins.livereload());

	});

	gulp.task('bower', function() {
		return gulp.src(mainBowerFiles())
				.pipe(plugins.uglify())
        		.pipe(gulp.dest(paths.bower.dest));
	});
	
	gulp.task('css', function() {
		
		gulp.src(paths.css.path+"/font/*", { base: './dev/css' })
			.pipe(gulp.dest(paths.css.dest+'/css/'));
		
		gulp.src(paths.css.origin)
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.plumber())
			.pipe(plugins.sass({
				outputStyle: 'compressed',
				sourceComments: 'map',
				includePaths : [paths.css.path].concat(neat)
			}))
			.pipe(plugins.autoprefixer(
				'last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'
			))
			.pipe(plugins.sourcemaps.write())
			.pipe(gulp.dest(paths.css.dest))
			.pipe(plugins.livereload());

	});
	
	gulp.task('images', function() {

		gulp.src([paths.images.origin+'/*.png',paths.images.origin+'/*.jpg',paths.images.origin+'/*.gif',paths.images.origin+'/*.jpeg'])
			.pipe(plugins.plumber())
			.pipe(plugins.imageOptimization({
				optimizationLevel: 5,
				progressive: true,
				interlaced: true
			}))
			.pipe(gulp.dest(paths.images.dest));
			
		gulp.src(paths.svg.origin)
			.pipe(plugins.plumber())
			.pipe(plugins.svgmin())
			.pipe(gulp.dest(paths.svg.dest));

	});

// WATCH FILES
// ==========================================================

	gulp.task('default', function () {
		plugins.livereload.listen();

		gulp.run('clean-images');

		gulp.run('bower');
		gulp.watch(paths.js.path+'/bower', ['bower']);

		gulp.run('js');
		gulp.watch(paths.js.origin, ['js']);
		
		gulp.run('css');
		gulp.watch(paths.css.origin, ['css']);
		
		gulp.run('images');
		plugins.watch(paths.images.origin, function (files) {
			gulp.start('images');
		});

	});